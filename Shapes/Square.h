#pragma once
#include "Shape.h"
class Square :
	public Shape
{
public:
	// Force client code to provide length
	Square(float length);

	// This is only accessible via 'Square' variable
	float getLength();

	// This is accessible from the 'Shape' variable
	// Override means we are just changing the implementation of the base type.
	float area() override;

private:
	// Data exclusive to this class
	float mLength;
};
