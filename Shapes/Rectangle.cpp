#include "Rectangle.h"

Rectangle::Rectangle(float length, float width)
	: Shape("Rectangle", 4)
{
	mLength = length;
	mWidth = width;
}

float Rectangle::getLength()
{
	return mLength;
}

float Rectangle::getWidth()
{
	return mWidth;
}

float Rectangle::area()
{
	return mLength * mWidth;
}
