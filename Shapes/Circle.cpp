#include "Circle.h"

Circle::Circle(float radius)
	: Shape("Circle", 0)
{
	mRadius = radius;
}

float Circle::getRadius()
{
	return mRadius;
}

float Circle::area()
{
	return 3.14f * mRadius * mRadius;
}
