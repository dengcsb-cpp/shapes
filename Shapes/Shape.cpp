#include "Shape.h"

Shape::Shape(string name, int sides)
{
	mName = name;
	mSides = sides;
}

string Shape::getName()
{
	return mName;
}

int Shape::getSides()
{
	return mSides;
}

float Shape::area()
{
	return 0.0f;
}
