#pragma once
#include "Shape.h"
class Rectangle :
	public Shape
{
public:
	Rectangle(float length, float width);

	float getLength();
	float getWidth();

	// Override base class' area()
	float area() override;

private:
	float mLength;
	float mWidth;
};

