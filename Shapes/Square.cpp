#include "Square.h"

Square::Square(float length)
	: Shape("Square", 4) // Initialize base class constructor
{
	mLength = length;
}

float Square::getLength()
{
	return mLength;
}

float Square::area()
{
	// Since we have context now (we know that this is a Square),
	// we can now provide the formula for area
	return mLength * mLength;
}
