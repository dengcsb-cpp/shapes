#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
public:
	Circle(float radius);

	float getRadius();

	// Override base' area()
	float area() override;

private:
	float mRadius;
};

