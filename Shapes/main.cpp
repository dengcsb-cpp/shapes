#include <iostream>
#include <string>
#include <vector>

#include "Rectangle.h"
#include "Square.h"
#include "Circle.h"

using namespace std;

int main()
{
	// Create shapes
	vector<Shape*> shapes;

	// Instantiate shapes. You can implicitly upcast them if you prefer.
	Square* square = new Square(10.0f);
	Rectangle* rectangle = new Rectangle(3.0f, 2.0f);
	Circle* circle = new Circle(8.0f);
	
	// Pushing each derived shape to the vector changes its data type to the
	// base type 'Shape'
	shapes.push_back(square);
	shapes.push_back(rectangle);
	shapes.push_back(circle);
	
	// Print area of all shapes
	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << shape->getName() << "\tArea: " << shape->area() << endl;
	}

	// Delete allocated memory
	for (int i = 0; i < 3; i++)
	{
		delete shapes[i];
	}
	shapes.clear();

	return 0;
}

