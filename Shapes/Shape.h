#pragma once
#include <string>
#include <iostream>

using namespace std;

class Shape
{
public:
	// Since name and sides are fixed per shape, we should only be able to set this once.
	Shape(string name, int sides);

	// Accessors are still the go-to access point
	string getName();
	int getSides();

	// 'virtual' keyword makes this method polymorphic. Meaning, context can be added LATER.
	// '= 0' makes this an abstract function. This must be overriden before the class can be instantiated
	virtual float area() = 0;

private:
	// Since these properties are private, the derived class can only access these via getter
	// Note that the data is still there, it's just that we can't access them
	string mName;
	int mSides;
};
